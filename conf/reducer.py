
import sys

current_user = None
num_actions = 0
num_sessions = 0


for line in sys.stdin:
    try:
        name, action = line.strip().split('\t')
    except:
        continue
    if current_user != name:
        if current_user and num_sessions:
            print('{}\t{}\t{}'.format(current_user, 1. * num_actions / (num_sessions), num_sessions))
        current_user = name
        num_sessions = 0
        num_actions = 0
    if action == 'begin':
        num_sessions += 1
    elif action == 'command':
        num_actions += 1


if current_user == name and num_sessions:
    print('{}\t{}\t{}'.format(current_user, 1. * num_actions / num_sessions, num_sessions))
