
import sys
import re


begin_regex = re.compile('.* UUID of player (\w+) is')
end_regex = re.compile('.* Disconnecting com.mojang.authlib.GameProfile.*name=(\w+),')
end_regex2 = re.compile('.* (\w+) lost connection: Disconnected')
action_regex = re.compile('.* (\w+) issued server command')


for line in sys.stdin:
    line = line.strip()
    if not line:
        continue

    begin_match = begin_regex.match(line)
    end_match = end_regex.match(line)
    end_match2 = end_regex2.match(line)
    action_match = action_regex.match(line)

    if begin_match:
        player_name = begin_match.group(1)
        print('{}\t{}'.format(player_name, 'begin'))
    elif end_match:
        player_name = end_match.group(1)
        print('{}\t{}'.format(player_name, 'end'))
    elif end_match2:
        player_name = end_match2.group(1)
        print('{}\t{}'.format(player_name, 'end'))
    elif action_match:
        player_name = action_match.group(1)
        print('{}\t{}'.format(player_name, 'command'))
