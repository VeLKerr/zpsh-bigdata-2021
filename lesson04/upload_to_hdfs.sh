#! /usr/bin/env bash

if [ -z $1 ];
then
    echo "Please input the number of points"
    exit 0
fi

python3 ./generate.py $1
hdfs dfs -rm -r pi_points
hdfs dfs -put pi_points ./
